package br.ucsal.bes20212.poo.atividade.domain;

public class Item {
	int cod;
	String nome;
	float valor;
	
	@Override
	public String toString() {
		return "Item [cod=" + cod + ", nome=" + nome + ", valor=" + valor + "]";
	}
	
}
