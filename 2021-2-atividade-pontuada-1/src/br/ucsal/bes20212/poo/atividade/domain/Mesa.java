package br.ucsal.bes20212.poo.atividade.domain;

public class Mesa {
	int numero;
	String localizacao;
	int cadeiras;
	
	@Override
	public String toString() {
		return "Mesa [cod=" + numero + ", localizacao=" + localizacao + ", cadeiras=" + cadeiras + "]";
	}
	
	
}
