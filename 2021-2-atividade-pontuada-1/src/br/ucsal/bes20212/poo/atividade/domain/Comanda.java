package br.ucsal.bes20212.poo.atividade.domain;

import java.util.ArrayList;

public class Comanda {
	
	int numero;
	Mesa mesa;
	ArrayList<Item> itens;
	int qtdItem;
	String situacao;
	
	@Override
	public String toString() {
		return "Comanda [numero=" + numero + ", mesa=" + mesa + ", itens=" + itens + ", qtdItem=" + qtdItem + ", situacao="
				+ situacao + "]";
	}

	
}
	